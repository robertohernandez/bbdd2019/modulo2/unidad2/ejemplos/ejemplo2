<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
   
        return $this->render('index');
    }
    public function actionAutor(){
        //cargar en el modelo lo que venga del formulario
//        $dato = Yii::$app->request->post();
//        $modelo = new \app\models\Autores();
//        $modelo->load($dato);
//        $modelo->save();
        
        // buscando el primer registro de autores
//        $dato = \app\models\Autores::find()->one();
        
//        echo "<pre>";
//        var_dump($modelo);
//        echo "</pre>";
//        
//        $id=50;
//        $modelo = \app\models\Autores::find()->where("id=50")->one();
//        if($modelo->load(Yii::$app->request->post())){
//            $modelo->save();
//        }
        
        
        return $this->render("autores",[
            'model'=>$modelo, 
        ]);
    }



 
}
