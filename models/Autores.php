<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "autores".
 *
 * @property int $id
 * @property string $nombre
 * @property string $email
 * @property string $fechaNacimiento
 *
 * @property Libros[] $libros
 */
class Autores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'autores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fechaNacimiento'], 'safe'],
            [['nombre'], 'string', 'max' => 200],
            [['email'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'email' => 'Email',
            'fechaNacimiento' => 'Fecha Nacimiento',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLibros()
    {
        return $this->hasMany(Libros::className(), ['autor' => 'id']);
    }
    
    //modelo para formatear una fecha convirtiendola a d/m/yyyy
    
    public function afterFind() {
        parent::afterFind();
        $this->fechaNacimiento=Yii::$app->formatter->asDate($this->fechaNacimiento, 'php:d-m-Y');;
        
    }
    // modificar la fecha que antes de grabar la devuelva en formato YYYY/m/d
    public function beforeSave($insert) {
        if(parent::beforeSave($insert)){
           $this->fechaNacimiento=Yii::$app->formatter->asDate($this->fechaNacimiento, 'php:Y-m-d');
           return true;
        }
       
      
    }
}
