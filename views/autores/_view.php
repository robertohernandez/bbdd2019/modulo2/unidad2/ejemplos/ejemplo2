<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Autores */


?>
<div class="autores-view">


    
    <!--cargamos una imagen--> 
<!--     Html::img('@web/imgs/'.$model->imagen, ['class' => 'img-thumbnail','style'=>'width:150px;float:right'])-->
    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nombre',
            'email:email',
            'fechaNacimiento', 
        ],
    ]) ?>

</div>
