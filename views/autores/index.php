<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AutoresSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Autores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="autores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Autores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            'email:email',
            'fechaNacimiento',
            'imagen',
            [
              'label'=>'foto del autor',
               'format'=>'raw',
              'value'=> function($model){//$model es una simple variable se llama así como se podia llamar de otra manera simplemente trae los datos del dataProvider
                return Html::img('@web/imgs/'.$model->imagen, ['class' => 'img-thumbnail','style'=>'width:150px;float:right']);
              }
            ],

         
            [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update} {delete} {libros}',
            'buttons' => [
                'libros' => function ($url,$model) {
               
                    return Html::a(
                       
                        '<span class="glyphicon glyphicon-book"></span>', 
                        ['libros/libros','id'=>$model->id]);
                },
                
	        ],
        ],
    ],        
    ]); ?>

 <?= Html::a('Active Record', ['site/consulta2a'], ['class' => 'btn btn-primary']) ?>
</div>
