<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Libros del Autor'.$autor->nombre;
$atras=Yii::$app->request->referrer;// guarda la ruta completa que hemos seguido para poder retroceder páginas.
$this->params['breadcrumbs'][] = ['label' => 'Autores', 'url' => $atras];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="libros-index">

    <h1><?= Html::encode($this->title) ?></h1>

<?=$this->render('//autores/_view',[ // se pone "//"autores pq es una vista dentro de otra
    'model'=>$autor,
])?>
    
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'titulo',
            'sinopsis',
            'fecha',
            'autor',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
